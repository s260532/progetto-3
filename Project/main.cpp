#include "GenericDomain.hpp"
#include "GenericMesh.hpp"
#include "Domain3D.hpp"
#include "MeshImport_Tetgen.hpp"
#include "FinderTetra.hpp"
#include "Cutter3D.hpp"
#include <iostream>

using namespace GeDiM;
using namespace Eigen;

int main(int argc, char** argv)
{
	/// CREATE DOMAIN
	unsigned int id = 1;

    Parallelepiped domain(id);
    Vector3d origin(0.0, 0.0, 0.0), length(1.0, 0.0, 0.0), height(0.0, 0.0, 1.0), width(0.0, 1.0, 0.0);

    domain.BuildParallelepiped(origin, length, height, width);

    MeshImport_Tetgen meshImportTetgen;
    meshImportTetgen.SetMinimumNumberOfCells(1);
    GenericMesh mesh;
    meshImportTetgen.CreateTetgenInput(domain);
    meshImportTetgen.CreateTetgenOutput(domain);
    meshImportTetgen.CreateMesh(domain, mesh);
    Output::PrintGenericMessage("Number of cell before the cutter %d", true, mesh.NumberOfCells());
    /// CREATE DOMAIN
	const unsigned int numDomainVertices = 4;
	GenericDomain2D domain2D(0,numDomainVertices);
	vector<Vector3d> vertexCoords(numDomainVertices);
	vertexCoords[0] << 0.2, 0.2, 0.5;
	vertexCoords[1] << 0.5, 0.2, 0.5;
	vertexCoords[2] << 0.5, 0.5, 0.5;
	vertexCoords[3] << 0.2, 0.5, 0.5;
	for(unsigned int i = 0; i < numDomainVertices; i++)
	{
		domain2D.AddVertex(vertexCoords[i]);
		domain2D.AddEdge(i, (i+1)%numDomainVertices);
    }
	domain2D.Initialize();

	mesh.ComputeGeometricalProperties();
	CutterMesh3D cutter;
	cutter.SetMesh(mesh);
	cutter.Initialize(1);
	cutter.AddDomain(domain2D);
	cutter.CutMesh();
	mesh.CleanInactiveTreeNode();
	Output::PrintGenericMessage("Number of cell after the cutter %d", true, mesh.NumberOfCells());
	/// FINDER TETRA
	FinderTetra finder;
	finder.SetMesh(mesh);
	finder.FindCell(domain2D);

	const list<GenericCell*> cellFound = finder.CellFound();
	/// OUTPUT MESH TO MATLAB SCRIPT FOR VISUALIZATION
	ofstream file("plotCellFound.m", ofstream::out );
	if(file.is_open())
	{
        file << "nodes = [";
        for(unsigned int i = 0; i < mesh.NumberOfPoints(); i++)
            file << mesh.Point(i)->Coordinates()(0) << "," <<  mesh.Point(i)->Coordinates()(1) << "," <<  mesh.Point(i)->Coordinates()(2) << ";" << endl;
        file << "];" << endl;

        file << "connectivity = [";
        for(list<GenericCell*>::const_iterator it = cellFound.begin(); it != cellFound.end(); it++)
        {
            GenericCell& cell = *(*it);
            file << cell.Point(0)->Id()+1 << "," <<  cell.Point(1)->Id()+1 << "," << cell.Point(2)->Id()+1 << "," << cell.Point(3)->Id()+1 << ";" << endl;
        }
        file << "];" << endl;
        file << "tetramesh(connectivity, nodes);" << endl;
        file.close();
	}
	else
        Output::PrintErrorMessage("Unable to open the file", true);

}
